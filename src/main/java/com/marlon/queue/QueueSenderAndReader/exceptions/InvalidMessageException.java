package com.marlon.queue.QueueSenderAndReader.exceptions;

public class InvalidMessageException extends RuntimeException {

    public InvalidMessageException(){
        super("INVALID MESSAGE");
    }
}
