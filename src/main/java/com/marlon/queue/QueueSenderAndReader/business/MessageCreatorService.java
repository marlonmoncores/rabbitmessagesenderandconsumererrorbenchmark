package com.marlon.queue.QueueSenderAndReader.business;

import com.marlon.queue.QueueSenderAndReader.queue.channel.DefaultMessageChannel;
import com.marlon.queue.QueueSenderAndReader.queue.data.MessageAvailable;
import com.marlon.queue.QueueSenderAndReader.web.data.MessageCreatorTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class MessageCreatorService {

    @Autowired
    private DefaultMessageChannel defaultMessageChannel;

    public void createMessages(MessageCreatorTO messageCreatorTO) {
        Random generator = new Random(messageCreatorTO.getQuantity());
        for (int i = 1; i<=messageCreatorTO.getQuantity(); i++) {
            defaultMessageChannel.sendMessage().send(
                    MessageBuilder.withPayload(new MessageAvailable(
                            randomStatus(generator, messageCreatorTO.getErrorRatio()),i==messageCreatorTO.getQuantity())
                    ).build()
            );
        }
    }

    private static MessageAvailable.STATUS randomStatus(Random generator, Double errorRatio){
        return generator.nextDouble() < errorRatio ? MessageAvailable.STATUS.FAIL : MessageAvailable.STATUS.SUCCESS;
    }
}


