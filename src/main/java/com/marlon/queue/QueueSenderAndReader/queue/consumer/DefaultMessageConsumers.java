package com.marlon.queue.QueueSenderAndReader.queue.consumer;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface DefaultMessageConsumers {

    String BASIC_QUEUE_WITH_DLQ = "basicQueueWithDlq";
    String BASIC_QUEUE_NO_RETRY = "basicQueueNoRetry";
    String BASIC_QUEUE_NO_RETRY_AND_DLQ_TTL_AND_MAX_RETRIES = "basicQueueNoRetryAndDlqTtlAndMaxRetries";


    @Input(BASIC_QUEUE_WITH_DLQ)
    SubscribableChannel basicQueueWithDlq();

    @Input(BASIC_QUEUE_NO_RETRY)
    SubscribableChannel basicQueueNoRetry();

    @Input(BASIC_QUEUE_NO_RETRY_AND_DLQ_TTL_AND_MAX_RETRIES)
    SubscribableChannel basicQueueNoRetryAndDlqTtlAndMaxRetries();
}
