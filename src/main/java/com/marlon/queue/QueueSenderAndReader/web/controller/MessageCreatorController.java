package com.marlon.queue.QueueSenderAndReader.web.controller;

import com.marlon.queue.QueueSenderAndReader.business.MessageCreatorService;
import com.marlon.queue.QueueSenderAndReader.web.data.MessageCreatorTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/message")
public class MessageCreatorController {

    @Autowired
    private MessageCreatorService messageCreatorService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createMessages(@RequestBody @Validated  MessageCreatorTO messageCreatorTO) {
        messageCreatorService.createMessages(messageCreatorTO);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
