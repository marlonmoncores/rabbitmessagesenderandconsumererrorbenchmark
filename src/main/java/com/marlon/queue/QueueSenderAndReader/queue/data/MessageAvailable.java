package com.marlon.queue.QueueSenderAndReader.queue.data;

import lombok.Data;

@Data
public class MessageAvailable {
    private final Long timestamp = System.currentTimeMillis();
    private STATUS status;
    private boolean last;

    public enum STATUS {FAIL, SUCCESS}

    public MessageAvailable (STATUS status, boolean last){
        this.status = status;
        this.last = last;
    }
}
