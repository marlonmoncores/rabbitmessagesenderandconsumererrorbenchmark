# RabbitMessageSenderAndConsumerErrorBenchmark
Created to test some available approaches trying to redeliver failed messages without compromising the overall delivery throughtput.

#Running the project
just type:
`make run`

##Prerequisites
- Make
- Maven
- Docker
- Java 8 SDK

 

##Important reference to the best approach found:
[Spring cloud documentation](http://cloud.spring.io/spring-cloud-static/spring-cloud-stream-binder-rabbit/2.1.2.RELEASE/multi/multi__retry_with_the_rabbitmq_binder.html)