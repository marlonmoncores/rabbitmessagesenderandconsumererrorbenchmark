package com.marlon.queue.QueueSenderAndReader.queue.publisher;

import com.marlon.queue.QueueSenderAndReader.queue.channel.DefaultMessageChannel;
import org.springframework.cloud.stream.annotation.EnableBinding;

@EnableBinding(DefaultMessageChannel.class)
public class DefaultMessagePublisher {
}
