package com.marlon.queue.QueueSenderAndReader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QueueSenderAndReaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(QueueSenderAndReaderApplication.class, args);
    }

}

