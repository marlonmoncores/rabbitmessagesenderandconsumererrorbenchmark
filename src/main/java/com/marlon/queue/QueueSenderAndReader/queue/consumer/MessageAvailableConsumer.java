package com.marlon.queue.QueueSenderAndReader.queue.consumer;

import com.marlon.queue.QueueSenderAndReader.business.MessageConsumerService;
import com.marlon.queue.QueueSenderAndReader.queue.data.MessageAvailable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.ImmediateAcknowledgeAmqpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

import java.util.Map;

@EnableBinding(DefaultMessageConsumers.class)
@Slf4j
public class MessageAvailableConsumer {

    @Autowired
    MessageConsumerService messageConsumerService;

    @StreamListener(target = DefaultMessageConsumers.BASIC_QUEUE_WITH_DLQ)
    public void basicQueueWithDlq(@Payload MessageAvailable message) {
        logTimeTakenToProcessQueue(message, DefaultMessageConsumers.BASIC_QUEUE_WITH_DLQ);
        messageConsumerService.consumeMessage(message);
    }

    @StreamListener(target = DefaultMessageConsumers.BASIC_QUEUE_NO_RETRY)
    public void basicQueueNoMaxAttempts(@Payload MessageAvailable message) {
        logTimeTakenToProcessQueue(message, DefaultMessageConsumers.BASIC_QUEUE_NO_RETRY);
        messageConsumerService.consumeMessage(message);
    }

    @StreamListener(target = DefaultMessageConsumers.BASIC_QUEUE_NO_RETRY_AND_DLQ_TTL_AND_MAX_RETRIES)
    public void basicQueueNoMaxAttemptsAndDlqTtlAndMaxRetries(@Payload MessageAvailable message,
                                                              @Header(name = "x-death", required = false) Map<?,?> death) {
        logTimeTakenToProcessQueue(message, DefaultMessageConsumers.BASIC_QUEUE_NO_RETRY_AND_DLQ_TTL_AND_MAX_RETRIES);
        throwExceptionAfterMaximumRetries(death);
        messageConsumerService.consumeMessage(message);
    }

    private void logTimeTakenToProcessQueue(MessageAvailable message, String name){
        if(message.isLast()){
            log.info("Queue {} took {} seconds reach last message",name, (System.currentTimeMillis() - message.getTimestamp()));
        }
    }

    private void throwExceptionAfterMaximumRetries(Map<?,?> death){
        if (death != null && death.get("count").equals(3L)) {
            // giving up - don't send to DLX
            throw new ImmediateAcknowledgeAmqpException("Failed after 4 attempts");
        }
    }
}
