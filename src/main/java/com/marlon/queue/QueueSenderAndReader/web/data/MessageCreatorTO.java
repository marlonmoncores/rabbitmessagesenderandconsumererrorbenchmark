package com.marlon.queue.QueueSenderAndReader.web.data;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class MessageCreatorTO {

    @NotNull
    private Integer quantity;

    @NotNull
    private Double errorRatio;

}
