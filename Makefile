DEBUG_5005=-Dspring-boot.run.jvmArguments="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005"

compose-up:
	docker-compose up -d

run: compose-up
	mvn clean spring-boot:run

run-debug: compose-up
	mvn clean spring-boot:run $(DEBUG_5005)
