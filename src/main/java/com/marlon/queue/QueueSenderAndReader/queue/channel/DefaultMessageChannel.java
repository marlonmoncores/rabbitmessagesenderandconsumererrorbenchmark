package com.marlon.queue.QueueSenderAndReader.queue.channel;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface DefaultMessageChannel {

    @Output("defaultMessageChannel")
    MessageChannel sendMessage();
}
