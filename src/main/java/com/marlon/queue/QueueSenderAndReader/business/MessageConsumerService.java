package com.marlon.queue.QueueSenderAndReader.business;

import com.marlon.queue.QueueSenderAndReader.exceptions.InvalidMessageException;
import com.marlon.queue.QueueSenderAndReader.queue.data.MessageAvailable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MessageConsumerService {


    public void consumeMessage(MessageAvailable messageAvailable) {
        if(MessageAvailable.STATUS.FAIL.equals(messageAvailable.getStatus())){
            log.debug("MESSAGE FAILED");
            throw new InvalidMessageException();
        }
        log.debug("MESSAGE READ");
    }
}


